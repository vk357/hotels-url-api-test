
// модули node и из node_models
var http = require('http');
var async = require('async');
var querystring = require('querystring');
var format = require('format');
var path = require('path');
var fs = require('fs');

// модули приложения
var log = require('./libs/logger')(module); // Логгирование
var errLog = require('./libs/err_logger')(module); // Логгирование
var config = require('./config'); // Конфигурация
var requests = require('./requests'); // Запросы и их параметры
var utilRandom = require('./libs/util_random'); // Генерация случайных параметров к запросам


var logsPath = path.join(''+__dirname, '/logs/'); // Создание лог папки
if (!fs.existsSync(logsPath)) {
  fs.mkdirSync(logsPath);
};

log.info('___________________________________________________________________');
log.debug('Init script!');

// авторизация
var authData = querystring.stringify(config.auth); // Форматирование строки запроса Авторизации

var authOptions = { // Опции Авторизации

  hostname: config.hostname,
  port: config.port,
  path: config.login_url,
  method: "POST",
  auth: config.auth.username + ':' + config.auth.password,  // данные для авторизации запросов
  headers: {
    'Content-Type': 'application/x-www-form-urlencoded',
    // 'Connection': 'keep-alive',
    'Content-Length': authData.length, 
    'User-Agent': 'EasyOnPad test nodejs bot'
  }

};

var cookieList = []; // Список параметров в куки

var listFunc = []; // Список функций-запросов, которые выполняются параллельно/последовательно

function getCoookie(callback) { // Функция получения Куки из заголовка ответа авторизации

  var l = cookieList.length;
  var cookieStr = "";

  for (var i = 0; i < l; i++) {
    cookieStr += cookieList[i].split(';')[0];
    if (i != l-1) {
      cookieStr += '; ';
    };
  };

  log.debug('Got cookie!');
  callback(null, cookieStr);

};


function createListOfFunc(cookieStr, callback) { /* Генерация списка функций, где 
  каждая функция выполняет свой запрос со своими параметрами, настройки беруться из
  request.json */
  for (var i = 0; i < requests.length; i++) { // Цикл по всем запросам

    listFunc.push( // Добавление объекта-функции в конец списка

      function(callback2) {
        /* В зависимости от настроек значения параметров данные к запросам, 
        либо случайно генерируются, либо беруться из параметров. 
        arguments.callee - ссылка внутри функции на саму себя. 
        listFunc.indexOf(arguments.callee) получение индекса текукщей функции 
        в списке выполняемых функций, этот индекс равен индексу соответствующего 
        запроса в requests.json */
        var data = config.random_query ? 
          querystring.stringify(
            utilRandom.randomQuery(requests[listFunc.indexOf(arguments.callee)])
          ) :
          querystring.stringify(
            requests[listFunc.indexOf(arguments.callee)].params
          );

        /* Аналогично с параметрами в Url запросах */
        var path = config.random_urlparam ? 
            format.vsprintf(
              requests[listFunc.indexOf(arguments.callee)].url, 
              utilRandom.randomUrlParams()
            ) :
            requests[listFunc.indexOf(arguments.callee)].url;

        var options = { // Настройки запроса
          hostname: config.hostname,
          port: config.port,
          path: path,
          method: requests[listFunc.indexOf(arguments.callee)].method,
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            // 'Connection': 'keep-alive',
            'Content-Length': data.length, 
            'User-Agent': 'EasyOnPad test nodejs bot',
            'Cookie': cookieStr
          }
        };
        

        var req = http.request(options, function(res) { // Объект запроса

          var resStr = ""; // Будующий результат
          resStr += "StatusCode: " + res.statusCode + "; "; // Статус ответа
          resStr += "Path: " + options.path + "; "; // Запрашиваемый url
          resStr += "Method: " + options.method + "; "; // Метод
          resStr += "Query: " + data + "; "; // Переданные параметры

          var resData = "";

          res.setEncoding('utf8');

          res.on('data', function(chunk) {
            resData += chunk; // Ответ. Он может приходить по кусочкам, поэтому используется собыите end
          });

          res.on('end', function() { // Окончание ответа на запрос
            if (config.show_response) {
              resStr += "Response: " + resData; // в зависимости от настроек, запись ответа
            };
            log.info(resStr);
            log.info('finish function');
            callback2(null, 'ok');
          });

        });

        req.on('error', function(e) { // При возникновении ошибки в ходе запроса
          errLog.errLog('"createListOfFunc[' + i + ']" FATAL ERROR: ' + e.message);
          delete req;
        })

        req.write(data); // Отправка запроса
        req.end();
        
      }

    );
    log.debug('Added new Func');
  };

  callback(null, listFunc);

};


function parallelTest(listFunc, callback) { // Запуск функции теста

  startTesting();
  callback(null, 'ok!!!');

};


function startTesting() { // Функция тестирования

  if (config.parallels) { // В зависимости от настроек

    log.info('Start "parallel" async');

    async.parallelLimit( // Запуск функций-запросов ко всем url в requests.json
      listFunc,          // параллельно(все запросы стартуют одновременно)
      config.limit,      // Ограничение на максимальное кол-во одновременных запросов

      function(err, results){ // По завершению выполнения ВСЕХ запросов
        if (err) {
          errLog.error('"async.parallel" FATAL ERROR: ' + err.message);
        } else {
          setTimeout(startTesting, config.interval); // Вызов новой порции запросов, через определённый интервал
          log.debug('finish ALL ' + listFunc.length + ' functions');
        }
        delete authReq;
      }
    );

  } else {

    log.info('Start "series" async');

    async.series( // Запуск функций-запросов ко всем url в requests.json
      listFunc,   // Последовательно(Новый запрос не начнётся, пока не будет ответа от предыдущего)

      function(err, results){ // По завершению выполнения Последнего запроса
        if (err) {
          errLog.error('"async.series" FATAL ERROR: ' + err.message);
        } else {
          setTimeout(startTesting, config.interval); // Вызов новой порции запросов, через определённый интервал
          log.debug('finish ALL ' + listFunc.length + ' functions '+results);
        }
        delete authReq;
      }
    );

  };
};


var authReq = new http.request(authOptions, function(res){ // Объект-запрос на авторизацию

  log.info('logged in as "' + config.auth.username + '"');

  cookieList = res.headers['set-cookie']; // По получению ответа, достать из заголовка параметры Кук

  async.waterfall([     // Последовательное выполнение с передачей параметров
      getCoookie,       // Форматирование Кук
      createListOfFunc, // Генерация функций запросов
      parallelTest      // Запуск тестирования
    ], function(err, results){
      if (err) {
        errLog.error('"async.waterfall" FATAL ERROR: ' + err.message);
      } else {
        log.debug('Finish init script! Waiting async work and collector.');
      }
      log.info(results);
      delete authReq;
    }
  );

});

authReq.on('error', function(e) { // При возникновении ошибки в запросе авторизации(не 500 ошибка)
  errLog.error('"authReq" FATAL ERROR: ' + e.message);
});

authReq.write(authData); // Отправка данных
authReq.end();