var randomList = '1234567890-=+*/asdfghjkl;zxcvbnm,.!@#$%^&*(){}[]:"| _'; // Набор сиволов для случайной генерации
var randNum = randomList.length;

exports.randomQuery = function(request){ // Генерация случайных параметров: { param_name: param_value, ... }
  var newParams = {};

  var keys = Object.keys(request.params);
  for (var i = 0; i < keys.length; i++) {
    newParams[keys[i]] = "" + 
        randomList[ Math.floor(Math.random() * (randNum - 0 + 1)) ];
  };

  return newParams;
};

exports.randomUrlParams = function(){ // Генерация случайных URL параметров 
  var newParams = [];

  newParams.push(
      randomList[ Math.floor(Math.random() * (randNum - 0 + 1)) ]
  );
  newParams.push(
      randomList[ Math.floor(Math.random() * (randNum - 0 + 1)) ]
  );

  return newParams;
};
