var winston = require('winston');

module.exports = function() { // Логгер данных

  var transports = [
    new winston.transports.Console({
      timestamp: true,
      colorize: true,
      level: 'debug'
    }),

    new winston.transports.File({
      filename: 'logs/info.log',
      colorize: false,
      level: 'info',
      maxsize: 4*1024*1024,
      json: false

    })
  ];

  return new winston.Logger({ transports: transports });
};
